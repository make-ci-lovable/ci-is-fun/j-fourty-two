# j-fourty-two

## Build it

```
mvn compile assembly:single
# run it
java -jar target/fourty-two.1.0-SNAPSHOT.jar 
```

## Add to Maven registry

### Create a `settings.xml` file

> Authenticate with a CI job token in Maven: https://docs.gitlab.com/ee/user/packages/maven_repository/#authenticate-with-a-ci-job-token-in-maven

```xml
<settings>
    <servers>
        <server>
        <id>gitlab-maven</id>
        <configuration>
            <httpHeaders>
            <property>
                <name>Job-Token</name>
                <value>${env.CI_JOB_TOKEN}</value>
            </property>
            </httpHeaders>
        </configuration>
        </server>
    </servers>
</settings>
```

## Update the `pom.xml` file

### 1️⃣ add `<gitlabProjectId>` and `<gitlabURL>`  to `<properties>`

Like this:
```xml
<properties>
    <gitlabProjectId>👋 change with your project id 🖐️</gitlabProjectId>
    <gitlabURL>https://gitlab.com/api/v4</gitlabURL>
    <mainClass>acme.FourtyTwo</mainClass>
</properties>
```

### 2️⃣ add `<repository>` to `<repositories>`

```xml
<repository>
    <id>gitlab-maven</id>
    <url>${gitlabURL}/projects/${gitlabProjectId}/packages/maven</url>
</repository>
```

### 3️⃣ add `<repository>` and `<snapshotRepository>` to `<distributionManagement>`

> ref: https://datacadamia.com/maven/distribution_management

```xml
<repository>
    <id>gitlab-maven</id>
    <url>${gitlabURL}/projects/${gitlabProjectId}/packages/maven</url>
</repository>
<snapshotRepository>
    <id>gitlab-maven</id>
    <url>${gitlabURL}/projects/${gitlabProjectId}/packages/maven</url>
</snapshotRepository>
```

## `.gitlab-ci.yml`

```yaml
image: maven:latest

stages:
  - build
  - deploy

build:run:42:
  stage: build
  script:
    - mvn compile assembly:single
    - java -jar target/fourty-two.1.0-SNAPSHOT.jar 

deploy:42:
  stage: deploy
  script:
    - mvn deploy -gs ./settings.xml
```

